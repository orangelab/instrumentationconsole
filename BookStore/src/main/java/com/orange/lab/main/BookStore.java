package com.orange.lab.main;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@EnableCircuitBreaker
@Service
public class BookStore {

	private static final ArrayList<String> fixedBookList = new ArrayList<String>();

	static {
		fixedBookList.add("El señor de los anillos");
		fixedBookList.add("Ender el Genocida");
		fixedBookList.add("El quijote");
	}

	@Value(value = "${info.name:#{BookStoreApp}}")
	protected String applicationName;

	@HystrixCommand(fallbackMethod = "reliable")
	protected ArrayList<String> getBookStoreList() {
		ArrayList<String> result = new ArrayList<String>();
		result.add(applicationName);
		result.addAll(fixedBookList);
		return result;
	}

	public ArrayList<String> reliable() {
		ArrayList<String> result = new ArrayList<String>();
		result.add("Cloud Native Java (O'Reilly)");
		return result;
	}
}
