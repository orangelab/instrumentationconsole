//  PROJECT:        Biblio
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2016 by Wanadoo España, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package com.orange.lab.main;

import org.springframework.stereotype.Component;

// - CLASS IMPLEMENTATION ...................................................................................
@Component
public class AppConfig {
	private String	appName		= "NotNamed";
	private String	hostName	= "notdefined";

	public String getHostName() {
		return hostName;
	}

	public String getAppName() {
		return appName;
	}

	public AppConfig setAppName(String appName) {
		this.appName = appName;
		return this;
	}

	public AppConfig setHostName(String hostName) {
		this.hostName = hostName;
		return this;
	}

	public String getInstanceId() {
		return getAppName() + "." + getHostName();
	}
}
// - UNUSED CODE ............................................................................................
