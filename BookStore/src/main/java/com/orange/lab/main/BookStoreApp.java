package com.orange.lab.main;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.Timer;
import com.netflix.servo.annotations.DataSourceType;
import com.netflix.servo.annotations.Monitor;
import com.netflix.servo.monitor.Monitors;
import com.netflix.servo.publish.BasicMetricFilter;
import com.netflix.servo.publish.CounterToRateMetricTransform;
import com.netflix.servo.publish.MetricObserver;
import com.netflix.servo.publish.MonitorRegistryMetricPoller;
import com.netflix.servo.publish.PollRunnable;
import com.netflix.servo.publish.PollScheduler;
import com.netflix.servo.publish.graphite.GraphiteMetricObserver;

@EnableCircuitBreaker
@RestController
@SpringBootApplication
public class BookStoreApp extends DataSpringApplication {
	private final static Logger LOGGER = LoggerFactory.getLogger(BookStoreApp.class);

	public static void main(String[] args) {
		new SpringApplicationBuilder(BookStoreApp.class).web(true).run(args);
	}

	@Autowired
	private BookStore						bookStore;

	@Value(value = "${applicationHostName}")
	protected String						applicationHostName;
	@Value(value = "${info.name:#{BookStoreApp}}")
	protected String						applicationName;
	@Value(value = "${info.version:#{0.0.1}}")
	protected String						applicationVersion;

	// Servo monitoring data
	@Monitor(name = "totalRequests", type = DataSourceType.COUNTER)
	private final AtomicInteger	totalRequests			= new AtomicInteger(0);
	@Monitor(name = "totalProcessTime", type = DataSourceType.COUNTER)
	private final AtomicLong		totalProcessTime	= new AtomicLong(0L);

	// CodeHale monitors

	@PostConstruct
	public void postConstruct() {
		// Initialize the properties to store the instance data
		LOGGER.info("ApplicationName: " + applicationName);
		LOGGER.info("ApplicationVersion: " + applicationVersion);
		LOGGER.info("HostName: " + applicationHostName);
		initializeOrangeCore(new AppConfig().setAppName(applicationName).setHostName(applicationHostName));

		// Start Servo monitoring
		//		startServoGraphite();

		// CodeHale metrics initialization and definition
		startCodaHaleGraphite();
	}

	private void startCodaHaleGraphite() {
		startReporter();
	}

	private void startServoGraphite() {
		Monitors.registerObject("BookStoreMetrics", this);

		// Start Servo polling to Graphite
		PollScheduler scheduler = PollScheduler.getInstance();
		scheduler.start();

		String prefix = "servo.orangelab." + applicationName + "." + applicationHostName;
		String addr = "dsibss:2003";
		MetricObserver graphiteObserver = new GraphiteMetricObserver(prefix, addr);

		MetricObserver transform = new CounterToRateMetricTransform(graphiteObserver, 1, TimeUnit.MINUTES);
		PollRunnable task = new PollRunnable(new MonitorRegistryMetricPoller(), BasicMetricFilter.MATCH_ALL, transform);
		scheduler.addPoller(task, 15, TimeUnit.SECONDS);
	}

	// Register the service entry point
	@RequestMapping(value = "/booklist")
	public List<String> bookList(
			@RequestHeader(value = "X-app-name", defaultValue = "CallerNotReceived") String callerAppName) {
		// CodeHale timer
		final Timer.Context context = getResponses().time();
		// Servo timing container
		Instant startInstant = new Instant();
		try {
			// CodeHale call counter
			if (null == callerAppName) callerAppName = "CallerNotReceived";
			//			LOGGER.info("callerAppName: " + callerAppName);
			getRequests(callerAppName).mark();
			// Servo call counter
			totalRequests.incrementAndGet();
			return bookStore.getBookStoreList();
		} finally {
			// Add a fake time delay to make execution lapses bigger.
			sleepRandom();
			// CodeHale timing registration
			context.stop();
			// Servo timing registration
			Instant endInstant = new Instant();
			long delta = endInstant.minus(startInstant.getMillis()).getMillis();
			LOGGER.info("callerAppName: " + callerAppName + " - delta: " + delta);
			//			totalProcessTime.addAndGet(endInstant.minus(startInstant.getMillis()).getMillis());
			//			LOGGER.info("totalProcessTime: " + totalProcessTime);
		}
	}
}
