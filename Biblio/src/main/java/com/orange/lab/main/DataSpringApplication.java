//  PROJECT:        OrangeCoreSpringApplication
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@orange.com
//  COPYRIGHT:      (c) 2016 by Orange Spain, S.L., all rights reserved.

package com.orange.lab.main;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codahale.metrics.Counter;
import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import com.orange.lab.entrypoint.AppDataExporter;

// - CLASS IMPLEMENTATION ...................................................................................
@EnableEurekaClient
public class DataSpringApplication {
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	//	private static Logger					logger					= Logger.getLogger("com.orange.lab.main");
	private static DateTime				startupTime			= new DateTime();
	private static AppConfig			configuration		= null;

	// CodeHale metrics storage
	@Autowired
	private static MetricRegistry	metricsRegistry	= new MetricRegistry();

	protected static String getApplicationName() {
		return configuration.getAppName();
	}

	protected static String getHostName() {
		return configuration.getHostName();
	}

	private static String getInstanceId() {
		return configuration.getInstanceId();
	}

	protected static void initializeOrangeCore(AppConfig appConfig) {
		configuration = appConfig;
		// Start the reporting to Graphite
		//		startReporter();
	}

	public static void startReporter() {
		final Graphite graphite = new Graphite(new InetSocketAddress("dsibss", 2003));
		final GraphiteReporter reporter = GraphiteReporter.forRegistry(metricsRegistry)
				.prefixedWith("codehale.orangelab." + getInstanceId() + "." + getApplicationName())
				.convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS).filter(MetricFilter.ALL)
				.build(graphite);
		reporter.start(15, TimeUnit.SECONDS);

		// Add JMX reporter
		final JmxReporter jmxreporter = JmxReporter.forRegistry(metricsRegistry).build();
		jmxreporter.start();
	}
	//	private static MetricObserver createGraphiteObserver() {
	//		final String prefix = Config.getGraphiteObserverPrefix();
	//		final String addr = Config.getGraphiteObserverAddress();
	//		return rateTransform(async("graphite", new GraphiteMetricObserver(prefix, addr)));
	//	}

	// - F I E L D S
	private final HashMap<String, Meter>	requests	= new HashMap<String, Meter>();
	private Timer													responses	= null;

	// - W E B   E N T R Y   P O I N T S
	@CrossOrigin
	@RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public AppDataExporter applicationData() {
		AppDataExporter dataList = new AppDataExporter(startupTime);
		// Add to the exported data the Metrics collected on the last minute.
		return add1MinuteMetrics(dataList);
	}

	// - M E T H O D   S E C T I O N
	protected void sleepRandom() {
		try {
			// Get a random delay between 0 and 999 milliseconds
			int delay = Double.valueOf(Math.random() * 1000).intValue();
			Thread.sleep(delay);
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
	}

	// Register the hystrix.stream to publish hystrix data to the dashboard
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		return new ServletRegistrationBean(new HystrixMetricsStreamServlet(), "/hystrix.stream");
	}

	protected Meter getRequests(String callerAppName) {
		if (null == callerAppName) callerAppName = "CallerNotReceived";
		Meter met = requests.get(callerAppName);
		if (null == met) requests.put(callerAppName, metricsRegistry.meter("requests." + callerAppName));
		return requests.get(callerAppName);
	}

	protected Timer getResponses() {
		if (null == responses) responses = metricsRegistry.timer(MetricRegistry.name("elapsedTime"));
		return responses;
	}

	private AppDataExporter add1MinuteMetrics(AppDataExporter data) {
		SortedMap<String, Counter> counters = metricsRegistry.getCounters();
		for (String key : counters.keySet()) {
			Counter c = counters.get(key);
			data.addEntry(key, new Long(c.getCount()).toString());
		}
		// SortedMap<String, Gauge> gauges = metricRegistry.getGauges();
		// for ( String key : gauges.keySet()) {
		// Gauge g = gauges.get(key);
		// data.addEntry(key,new Long(g.getValue()).toString());
		// }
		SortedMap<String, Meter> meters = metricsRegistry.getMeters();
		for (String key : meters.keySet()) {
			Meter m = meters.get(key);
			data.addEntry(key, new Long(m.getCount()).toString());
		}
		return data;
	}
}

// - UNUSED CODE ............................................................................................
