package com.orange.lab.main;

import java.net.URI;
import java.util.logging.Logger;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orange.lab.entrypoint.XUserAgentInterceptor;

@Service
public class BookService {
	private static Logger logger = Logger.getLogger("com.orange.lab");

	@HystrixCommand(fallbackMethod = "reliable")
	public String readingList() {
		//		try {
		// RestTemplate restTemplate = new RestTemplate();
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
		// restTemplate.setInterceptors(Collections.singletonList(new XUserAgentInterceptor()));
		restTemplate.getInterceptors().add(new XUserAgentInterceptor(BiblioApp.getApplicationName()));
		URI uri = URI.create("https://aotlxtewsb00001:9061/bookstoreapp-v1/booklist");

		return restTemplate.getForObject(uri, String.class);
		//		} catch (Exception exc) {
		//			logger.warning(exc.getLocalizedMessage());
		//		}

		//		return "NO DATA - Exception";
	}

	public String reliable(Throwable exc) {
		logger.warning(exc.getMessage());
		return "Cloud Native Java (O'Reilly)";
	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(2000);
		factory.setConnectTimeout(2000);
		return factory;
	}
}
