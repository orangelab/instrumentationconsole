package com.orange.lab.main;

import java.net.URI;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.PostConstruct;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.codahale.metrics.Timer;
import com.netflix.servo.annotations.DataSourceType;
import com.netflix.servo.annotations.Monitor;
import com.netflix.servo.monitor.Monitors;
import com.netflix.servo.publish.BasicMetricFilter;
import com.netflix.servo.publish.CounterToRateMetricTransform;
import com.netflix.servo.publish.MetricObserver;
import com.netflix.servo.publish.MonitorRegistryMetricPoller;
import com.netflix.servo.publish.PollRunnable;
import com.netflix.servo.publish.PollScheduler;
import com.netflix.servo.publish.graphite.GraphiteMetricObserver;
import com.orange.lab.entrypoint.XUserAgentInterceptor;

@EnableCircuitBreaker
@RestController
@SpringBootApplication
public class BiblioApp extends DataSpringApplication {
	private final static Logger	LOGGER	= LoggerFactory.getLogger(BiblioApp.class);

	public static void main(String args[]) {
		new SpringApplicationBuilder(BiblioApp.class).web(true).run(args);
	}

	@Value(value = "${applicationHostName}")
	protected String						applicationHostName;
	@Value(value = "${info.name:#{BiblioApp}}")
	protected String						applicationName;
	@Value(value = "${info.version:#{0.0.1}}")
	protected String						applicationVersion;

	// Servo monitoring data
	@Monitor(name = "totalRequests", type = DataSourceType.COUNTER)
	private final AtomicInteger	totalRequests			= new AtomicInteger(0);
	@Monitor(name = "totalProcessTime", type = DataSourceType.COUNTER)
	private final AtomicLong		totalProcessTime	= new AtomicLong(0L);

	// CodeHale monitors

	@PostConstruct
	public void postConstruct() {
		// Initialize the properties to store the instance data
		LOGGER.info("ApplicationName: " + applicationName);
		LOGGER.info("ApplicationVersion: " + applicationVersion);
		LOGGER.info("HostName: " + applicationHostName);
		initializeOrangeCore(new AppConfig().setAppName(applicationName).setHostName(applicationHostName));

		// Start Servo monitoring
		//		startServoGraphite();

		// CodeHale metrics initialization and definition
		startCodaHaleGraphite();
	}

	private void startCodaHaleGraphite() {
		startReporter();
	}

	private void startServoGraphite() {
		Monitors.registerObject("BookStoreMetrics", this);

		// Start Servo polling to Graphite
		PollScheduler scheduler = PollScheduler.getInstance();
		scheduler.start();

		String prefix = "servo.orangelab." + applicationName + "." + applicationHostName;
		String addr = "dsibss:2003";
		MetricObserver graphiteObserver = new GraphiteMetricObserver(prefix, addr);

		MetricObserver transform = new CounterToRateMetricTransform(graphiteObserver, 1, TimeUnit.MINUTES);
		PollRunnable task = new PollRunnable(new MonitorRegistryMetricPoller(), BasicMetricFilter.MATCH_ALL, transform);
		scheduler.addPoller(task, 15, TimeUnit.SECONDS);
	}

	//	@Autowired
	//	private BookService bookService;
	// Release 0.3.0-Servo. Changing the definitions of all the metrics to be Servo declared and then 
	// accesible through JMXor other injectors.
	@RequestMapping("/booklist")
	public String bookList() {
		// CodeHale timer
		final Timer.Context context = getResponses().time();
		// Servo timing container
		Instant startInstant = new Instant();
		try {
			// CodeHale call counter
			//			if (null == callerAppName) callerAppName = "CallerNotReceived";
			//			LOGGER.info("callerAppName: " + callerAppName);
			getRequests(applicationName).mark();
			// Servo call counter
			totalRequests.incrementAndGet();
			return new GetBooksCommand().run();
			//			return new GetBooksCommand().execute();
		} finally {
			// Add a fake time delay to make execution lapses bigger.
			sleepRandom();
			// CodeHale timing registration
			context.stop();
			// Servo timing registration
			Instant endInstant = new Instant();
			long delta = endInstant.minus(startInstant.getMillis()).getMillis();
			LOGGER.info("delta: " + delta);
			//			totalProcessTime.addAndGet(endInstant.minus(startInstant.getMillis()).getMillis());
		}
	}
}

final class GetBooksCommand /* extends HystrixCommand<String> */{
	private final static Logger	LOGGER								= LoggerFactory.getLogger(GetBooksCommand.class);
	//	@Value(value = "${zuulserviceentrypoint.bookstoreapp}")
	protected String						zuulserviceentrypoint	= "https://AOTLXTEWSB00001.cosmos.es.ftgroup:9061/bookstoreapp-v1/booklist";

	protected GetBooksCommand() {
		//		super(HystrixCommandGroupKey.Factory.asKey("GetBooks"));
	}

	protected String run() /* throws Exception */{
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
		restTemplate.getInterceptors().add(new XUserAgentInterceptor(BiblioApp.getApplicationName()));
		// Call the microservice through Zuul interface
		URI uri = URI.create(zuulserviceentrypoint);

		return restTemplate.getForObject(uri, String.class);
	}

	//	protected String getFallback() {
	//		LOGGER.warn(getFailedExecutionException().getMessage());
	//		return "logged Cloud Native Java (O'Reilly)";
	//	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(2000);
		factory.setConnectTimeout(2000);
		return factory;
	}
}