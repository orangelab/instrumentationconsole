//	PROJECT:        Reader
//	AUTHORS:        Adam Antinoo - adamantinoo.git@gmail.com
//	COPYRIGHT:      (c) 2013-2016 by Dimensinfin Industries, all rights reserved.
//	ENVIRONMENT:		Android API15.
//	DESCRIPTION:		Application to get access to character data from Eve Online. Specialized on
//									industrial management.

package com.orange.lab.entrypoint;

import java.io.IOException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

// - CLASS IMPLEMENTATION ...................................................................................
//@PropertySource("classpath:application.yml")
@Component
public class XUserAgentInterceptor implements ClientHttpRequestInterceptor {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger	logger	= Logger.getLogger("com.orange.lab");

	// - F I E L D - S E C T I O N ............................................................................
	//	@Value("${application.name:Default}")
	private String				appName	= "FirstName";
	//	@Autowired
	//	@Value("${server.port}")
	//	private String				port;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	@Autowired
	public XUserAgentInterceptor(@Value("${application.name:Default}") String setAppName) {
		appName = setAppName;
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		HttpHeaders headers = request.getHeaders();
		if (null == appName) appName = "InternalDefault";
		headers.add("X-app-name", appName);
		return execution.execute(request, body);
	}
}

// - UNUSED CODE ............................................................................................
