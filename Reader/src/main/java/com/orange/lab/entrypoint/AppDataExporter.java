//  PROJECT:        OrangeCoreSpringApp
//  FILE NAME:      $RCSfile: ProcessorApp.java,v $
//  FILE PATH:      $Source: /docs/cvsroot/mecha/evtloop/com/alehop/evtloop/processor/ProcessorApp.java,v $
//  LAST UPDATE:    $Date: 2000/06/28 11:52:36 $
//  RELEASE:        $Revision: 1.4 $
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@es.wanadoo.com
//  LAST USER:      $Author: ldiego $
//  COPYRIGHT:      (c) 2016 by Wanadoo España, S.L., all rights reserved.
//  LOG:
//    $Log: ProcessorApp.java,v $

package com.orange.lab.entrypoint;

import java.io.Serializable;
import java.util.HashMap;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

// - CLASS IMPLEMENTATION ...................................................................................
public class AppDataExporter implements Serializable {
	private static Logger									logger						= Logger.getLogger("com.orange.lab.entrypoint");

	// - F I E L D S
	//	private final IAppConnector	connector;
	private DateTime											startupTime				= null;
	private long													startTimeinMillis	= 0;
	private final HashMap<String, String>	dataEntries				= new HashMap<String, String>();

	// - C O N S T R U C T O R S
	public AppDataExporter(DateTime startupTime) {
		this.startupTime = startupTime;
		startTimeinMillis = this.startupTime.getMillis();
		addEntry("startupTime", getStartupTime());
		addEntry("runningTime", getRunningTime());
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public void addEntry(String key, String value) {
		dataEntries.put(key, value);
	}

	public HashMap<String, String> getDataEntries() {
		return dataEntries;
	}

	private String getStartupTime() {
		return startupTime.toString();
	}

	//	public String getRequests() {
	//		DataSpringApplication.requests++;
	//		return Integer.toString(DataSpringApplication.requests);
	//	}

	//	public String getRequestsTime() {
	//		DataSpringApplication.requestsTime += new Long(new Double(Math.random() * 1000).intValue());
	//		return Long.toString(DataSpringApplication.requestsTime);
	//	}

	private String getRunningTime() {
		DateTime startTime = new DateTime(startTimeinMillis);
		DateTime now = new DateTime();
		Period period = new Period(startTime, now);

		PeriodFormatter formatter = new PeriodFormatterBuilder().appendYears().appendSuffix("y ").appendMonths()
				.appendSuffix("m ").appendDays().appendSuffix("d ").appendHours().appendSuffix("h ").appendMinutes()
				.appendSuffix("m ").appendSeconds().appendSuffix("s").printZeroNever().toFormatter();
		String p2 = period.toString(formatter);
		return p2;
	}

	//		public String getCompilationDate() {
	//			return DataSpringApplication.compilationDate.toString();
	//		}

}

// - UNUSED CODE ............................................................................................
