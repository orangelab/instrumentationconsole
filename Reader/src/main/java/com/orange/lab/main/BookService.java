package com.orange.lab.main;

import java.net.URI;

import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.orange.lab.entrypoint.XUserAgentInterceptor;

@Service
public class BookService {

	@HystrixCommand(fallbackMethod = "reliable")
	public String readingList() {
		// RestTemplate restTemplate = new RestTemplate();
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
		// restTemplate.setInterceptors(Collections.singletonList(new XUserAgentInterceptor()));
		restTemplate.getInterceptors().add(new XUserAgentInterceptor(ReaderApplication.appName));
		URI uri = URI.create("http://AOTLXTEWSB00001.cosmos.es.ftgroup:9060/bookstoreapp/booklist");

		return restTemplate.getForObject(uri, String.class);
	}

	public String reliable() {
		return "Cloud Native Java (O'Reilly)";
	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(2000);
		factory.setConnectTimeout(2000);
		return factory;
	}
}
