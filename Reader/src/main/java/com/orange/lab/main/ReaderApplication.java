package com.orange.lab.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.Timer;

@EnableCircuitBreaker
@RestController
@EnableEurekaClient
@SpringBootApplication
public class ReaderApplication extends DataSpringApplication {
	public static String appName = "Reader";

	public static void main(String args[]) {
		initializeOrangeCore(appName);
		SpringApplication.run(ReaderApplication.class, args);
	}

	@Autowired
	private BookService bookService;

	@RequestMapping("/to-read")
	public String toRead() {
		final Timer.Context context = getResponses().time();
		try {
			getRequests(appName).mark();
			return bookService.readingList();
		} finally {
			context.stop();
		}
	}
}
