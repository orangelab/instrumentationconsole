//  PROJECT:        OrangeCoreSpringApplication
//  AUTHORS:        Luis de Diego (LDD) - luis.dediego@orange.com
//  COPYRIGHT:      (c) 2016 by Orange Spain, S.L., all rights reserved.

package com.orange.lab.main;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import com.orange.lab.entrypoint.AppDataExporter;

// - CLASS IMPLEMENTATION ...................................................................................
//@EnableEurekaClient
public class DataSpringApplication {
	// - F I E L D - S E C T I O N ............................................................................
	// - G L O B A L - C O N S T A N T S
	private static Logger					logger					= Logger.getLogger("com.orange.lab.main");
	@Autowired
	private static MetricRegistry	metricRegistry	= new MetricRegistry();
	private static DateTime				startupTime			= new DateTime();
	private static String					appName					= "NotNamed";

	// - M E T H O D - S E C T I O N ..........................................................................
	protected static void initializeOrangeCore(String newApplicationName) {
		appName = newApplicationName;
		// Start the reporting to Graphite
		startReport();
	}

	private static String getApplicationName() {
		return appName;
	}

	private static void startReport() {
		final Graphite graphite = new Graphite(new InetSocketAddress("swfbilling", 2003));
		final GraphiteReporter reporter = GraphiteReporter.forRegistry(metricRegistry)
				.prefixedWith("orangelab." + getApplicationName()).convertRatesTo(TimeUnit.SECONDS)
				.convertDurationsTo(TimeUnit.MILLISECONDS).filter(MetricFilter.ALL).build(graphite);
		reporter.start(5, TimeUnit.SECONDS);
	}

	// - F I E L D S
	private final HashMap<String, Meter>	requests	= new HashMap<String, Meter>();
	private Timer													responses	= null;

	// - W E B   E N T R Y   P O I N T S
	@CrossOrigin
	@RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public AppDataExporter applicationData() {
		// try {
		// Thread.sleep(3000); //1000 milliseconds is one second.
		// } catch (InterruptedException ex) {
		// Thread.currentThread().interrupt();
		// }
		// BaseBookServiceApp.requests++;
		// BaseBookServiceApp.requestsTime += new Long(new Double(Math.random() * 1000).intValue());
		AppDataExporter dataList = new AppDataExporter(startupTime);
		// Add to the exported data the Metrics collected on the last minute.
		return add1MinuteMetrics(dataList);
	}

	// Register the hystrix.stream to publish hystrix data to the dashboard
	@Bean
	public ServletRegistrationBean servletRegistrationBean() {
		return new ServletRegistrationBean(new HystrixMetricsStreamServlet(), "/hystrix.stream");
	}

	// - M E T H O D   S E C T I O N
	protected Meter getRequests(String callerAppName) {
		if (null == callerAppName) callerAppName = "UnnamedApp";
		Meter met = requests.get(callerAppName);
		if (null == met) requests.put(callerAppName, metricRegistry.meter("requests." + callerAppName));
		return requests.get(callerAppName);
	}

	protected Timer getResponses() {
		if (null == responses) responses = metricRegistry.timer(MetricRegistry.name("responses"));
		return responses;
	}

	private AppDataExporter add1MinuteMetrics(AppDataExporter data) {
		SortedMap<String, Counter> counters = metricRegistry.getCounters();
		for (String key : counters.keySet()) {
			Counter c = counters.get(key);
			data.addEntry(key, new Long(c.getCount()).toString());
		}
		// SortedMap<String, Gauge> gauges = metricRegistry.getGauges();
		// for ( String key : gauges.keySet()) {
		// Gauge g = gauges.get(key);
		// data.addEntry(key,new Long(g.getValue()).toString());
		// }
		SortedMap<String, Meter> meters = metricRegistry.getMeters();
		for (String key : meters.keySet()) {
			Meter m = meters.get(key);
			data.addEntry(key, new Long(m.getCount()).toString());
		}
		return data;
	}
}

// - UNUSED CODE ............................................................................................
